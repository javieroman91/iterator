#pragma once

// https://www.youtube.com/user/dulimarta/videos

namespace kiral {

	template<typename Z>
	class vector {

	public: 
		// Fordward declaration of iterators
		class It;
		class ConstIt;

	public:
		// Default constructor
		vector() : _data(new Z[default_capacity]) { }

		// Custom constructor
		vector(const unsigned int size) : _size(size) { 
			_size == 0 ? _capacity = default_capacity : _capacity = size * 2;
			_data = new Z[_capacity];
		}

		// Copy constructor
		vector(const vector& v) : _size(v._size), _capacity(v._capacity), _data(new Z[v._capacity]) {
			for (size_t i = 0; i < _size; i++) {
				_data[i] = v._data[i];
			}
		}

		// Move constructor
		vector(vector&& v) {
			_size = v._size;
			_capacity = v._capacity;
			_data = v._data;

			// Release pointer from source so the destructor dos not free the memory multiple times
			v._data = nullptr;
			v._size = 0;
			v._capacity = default_capacity;
		}

		~vector() {
			delete[] _data;
			_data = nullptr;
		}

	public:
		void push_back(const Z& z) {
			if (_size == _capacity) {
				expand();
			}
			_data[_size] = z;
			_size++;
		}

		inline unsigned int size() const {
			return _size;
		}

		inline unsigned int capacity() const {
			return _capacity;
		}

		inline bool empty() const {
			return _size == 0;
		}

		void reserve(const unsigned int capacity) {

			if (capacity <= _capacity) {
				return;
			}

			_capacity = capacity;
			Z* new_data = new Z[_capacity];
			for (size_t i = 0; i < _size; i++) {
				new_data[i] = _data[i];
			}
			delete[] _data;
			_data = new_data;
		}

		void resize(const unsigned int size) {
			// Not implemented
		}

		void clear() {
			delete[] _data;
			_size = 0;
			_capacity = default_capacity;
			_data = new Z[_capacity];
		}

		inline Z* data() const {
			return _data;
		}

		// Assigment operator
		vector& operator=(const vector& v) {
			
			delete[] _data;
			_size = v._size;
			_capacity = v._capacity;
			_data = new Z[_capacity];
			for (size_t i = 0; i < _size; i++) {
				_data[i] = v._data[i];
			}
			return *this;
		}

		// Move assigment operator
		vector& operator=(vector&& v) { 
			if (this == v) {
				// Tries to assign to itself
				return *this;
			}

			// Free existing resource
			delete[] _data;

			// Copy data pointer from the source object
			_data = v._data;
			_size = v._size;
			_capacity = v._capacity;

			// Release pointer from source so the destructor dos not free the memory multiple times
			v._data = nullptr;
			v._size = 0;
			v._capacity = default_capacity;

			return *this;
		}

		const Z& operator[](const unsigned int x) const {
			return _data[x];
		}
		Z& operator[](const unsigned int x) {
			return _data[x];
		}

		It begin() {
			return It(&_data[0]);
		}
		It end() {
			return It(&_data[_size]);
		}
		It rbegin() {
			return It(&_data[_size - 1]);
		}
		It rend() {
			return It(&_data[-1]);
		}

		const ConstIt begin() const {
			return ConstIt(&_data[0]);
		}
		const ConstIt end() const {
			return ConstIt(&_data[_size]);
		}
		const ConstIt rbegin() const {
			return ConstIt(&_data[_size - 1]);
		}
		const ConstIt rend() const {
			return ConstIt(&_data[-1]);
		}


	protected:
		void expand() {
			_capacity == 0 ? _capacity = default_capacity : _capacity *= 2;
			Z* new_data = new Z[_capacity];
			for (size_t i = 0; i < _size; i++) {
				new_data[i] = _data[i];
			}
			delete[] _data;
			_data = new_data;
		}

	protected:
		unsigned int _size = 0;
		unsigned int _capacity = default_capacity;

		Z* _data = nullptr;

	private:
		static const unsigned int default_capacity = 2;
	}; // vector class


	template <typename Z>
	class vector<Z>::ConstIt {
	public:
		ConstIt(Z* init) {
			_current = init;
		}

		bool operator !=(const ConstIt& it) const {
			return this->_current != it._current;
		}
		// Could return void instead of the iterator but this is a better design
		ConstIt& operator++() { // Prefix ++
			_current++;
			return *this;
		}
		ConstIt& operator++(int unused) { // Postfix ++
			_current++;
			return *this;
		}
		const Z& operator*() const {
			return *_current;
		}

	private:
		Z* _current = nullptr;
	}; // ConstIt class


	template <typename Z>
	class vector<Z>::It {
	public:
		It(Z* init) {
			_current = init;
		}

		bool operator !=(const It& it) {
			return this->_current != it._current;
		}
		It& operator++() {
			_current++;
			return *this;
		}
		It& operator++(int unused) {
			_current++;
			return *this;
		}
		Z& operator *() {
			return *_current;
		}

	private:
		Z* _current = nullptr;
	}; // It class
}
