#include <iostream>
#include "my_class.h"

int main() {
	kiral::vector<double> v;

	std::cout << "Empty: " << v.empty() << std::endl;
	v.push_back(15.45);
	std::cout << "Empty: " << v.empty() << std::endl;

	for (auto it = v.begin(); it != v.end(); it++) {
		std::cout << *it << std::endl;
		*it += 20;
	}

	kiral::vector<double> v2(10);

	kiral::vector<double> v3(v);
	for (auto it = v3.begin(); it != v3.end(); it++) {
		std::cout << *it << std::endl;
	}

	v3.clear();
	v3.reserve(10);
	std::cout << "Empty: " << v3.empty() << std::endl;
	std::cout << "Capacity: " << v3.capacity() << std::endl;

	v3.push_back(56.43);
	for (auto it = v3.begin(); it != v3.end(); it++) {
		std::cout << *it << std::endl;
	}

	kiral::vector<double> v4;
	v4 = v3;

	for (auto it = v4.begin(); it != v4.end(); it++) {
		std::cout << *it << std::endl;
	}

	kiral::vector<double> v5(0);
	v5.push_back(45.23);

	return 0;
}
